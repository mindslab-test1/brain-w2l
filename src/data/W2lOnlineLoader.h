/**
 * Copyright (c) Facebook, Inc. and its affiliates.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree.
 */

#pragma once

namespace w2l {

  class W2lOnlineLoader{
  public:
    W2lOnlineLoader();
    ~W2lOnlineLoader();
    static W2lFeatureInputData load(const std::string& data, bool isRaw=false);
  };
} // namespace w2l
