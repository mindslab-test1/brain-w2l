/**
 * Copyright (c) Facebook, Inc. and its affiliates.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree.
 */

#include <unicode/normlzr.h>

#include "common/FlashlightUtils.h"
#include "data/Utils.h"
#include "data/Sound.h"

namespace w2l {

namespace {
  std::string normalize(const std::string &utf8, /* U_ICU_NAMESPACE:: */ UNormalizationMode mode);
}

std::string nfc(const std::string &utf8) {
  return normalize(utf8, UNORM_NFC);
}

std::string nfd(const std::string &utf8) {
  return normalize(utf8, UNORM_NFD);
}

std::string nfkc(const std::string &utf8) {
  return normalize(utf8, UNORM_NFKC);
}

std::string nfkd(const std::string &utf8) {
  return normalize(utf8, UNORM_NFKD);
}

namespace {
  inline std::string normalize(const std::string &utf8, /* U_ICU_NAMESPACE::*/ UNormalizationMode mode) {
    using U_ICU_NAMESPACE::UnicodeString;
    using U_ICU_NAMESPACE::StringPiece;
    //using U_ICU_NAMESPACE::UErrorCode;

    const UnicodeString source =
        UnicodeString::fromUTF8(
            StringPiece(utf8.c_str(), utf8.size()));
    UnicodeString result;
    UErrorCode status = U_ZERO_ERROR;
    U_ICU_NAMESPACE::Normalizer::normalize(source, mode, 0, result, status);
    if(U_FAILURE(status)) {
      throw std::runtime_error("Unicode normalization failed");
    }
    std::string tmp;
    return result.toUTF8String(tmp);
  }
}

NoiseAugment::NoiseAugment(
    const std::string& filenames,
    const double minSnr /* = 0 */,
    const double maxSnr /* = 0 */,
    const std::string& rootdir /* = "" */)
    : minSnr_(minSnr), maxSnr_(maxSnr) {

  std::random_device rd;
  rng_ = std::make_shared<std::mt19937>(rd());

  auto noiseFilesVec = split(',', filenames);
  for (const auto& f : noiseFilesVec) {
    auto fullpath = pathsConcat(rootdir, trim(f));
    std::ifstream infile(fullpath);
    std::string line;

    while (std::getline(infile, line)) {
      auto noisePath = splitOnWhitespace(line, true);
      data_.emplace_back(noisePath[1]);
    }
  }
}

std::vector<float> NoiseAugment::addNoise(const std::vector<float>& audio) const {
  long noiseIdx = generateRandomLong(0, data_.size());
  std::string noisePath = data_[noiseIdx];

  std::vector<float> noise = loadSound<float>(noisePath);
  std::shared_ptr<long> start;
  std::shared_ptr<long> end;
  noise = resizeNoise(noise, audio.size(), start, end);
  float noiseNorm = norm(noise, start, end);

  float audioNorm = norm(audio, start, end);

  float snr = generateSnr();

  std::vector<float> result(audio.size(), 0);
  float noiseFactor;
  if (noiseNorm == 0.0) {
    noiseFactor = 1;
  } else {
    noiseFactor = audioNorm / noiseNorm * powf(10, -snr / 20);
  }
  float resultMax = 0;
  for (unsigned long sampleIdx = 0; sampleIdx < audio.size(); sampleIdx++) {
    result[sampleIdx] = audio[sampleIdx] + noise[sampleIdx] * noiseFactor;
    float resultItem = fabsf(result[sampleIdx]);
    if (resultMax < resultItem){
      resultMax = resultItem;
    }
  }

  if (0.999 < resultMax) {
    resultMax /= 0.999;
    for (auto& sample : result) {
      sample /= resultMax;
    }
  }
  return result;
}

std::vector<float> NoiseAugment::randomCrop(const std::vector<float>& audio, long min, long max) const {
  if (max < min) {
    std::swap(min, max);
  }
  if (audio.size() < max) {
    max = audio.size();
  }
  if (audio.size() < min) {
    max = min;
  }

  long len = generateRandomLong(min, max + 1);
  std::shared_ptr<long> start;
  std::shared_ptr<long> end;
  return resizeNoise(audio, len, start, end);
}

std::vector<float> NoiseAugment::resizeNoise(
    const std::vector<float>& noise, long len, std::shared_ptr<long>& start, std::shared_ptr<long>& end) const {
  std::vector<float> result(len, 0);

  long noiseLen = noise.size();
  if (noiseLen < len) {
    long resultFrom = generateRandomLong(0, len - noiseLen + 1);
    std::copy(noise.begin(), noise.end(), result.begin() + resultFrom);
    start = std::make_shared<long>(resultFrom);
    end = std::make_shared<long>(resultFrom+noise.size());
  } else {
    long noiseFrom = generateRandomLong(0, noiseLen - len + 1);
    long noiseTo = noiseFrom + len;
    std::copy(noise.begin() + noiseFrom, noise.begin() + noiseTo, result.begin());
    start = std::make_shared<long>(0);
    end = std::make_shared<long>(len);
  }
  return result;
}

float NoiseAugment::norm(
    const std::vector<float>& audio, const std::shared_ptr<long>& start, const std::shared_ptr<long>& end) const {
  float result = 0;
  for (long audioIdx = *start; audioIdx < *end; audioIdx++) {
    result += (audio[audioIdx] * audio[audioIdx]);
  }
  return sqrtf(result);
}

long NoiseAugment::generateRandomLong(long low, long high) const {
  std::uniform_int_distribution<long> uniformDist(low, high - 1);
  return uniformDist(*rng_);
}

float NoiseAugment::generateSnr() const {
  std::uniform_real_distribution<float> uniformDist(minSnr_, maxSnr_);
  return uniformDist(*rng_);
}

std::vector<int64_t> sortSamples(
    const std::vector<SpeechSampleMetaInfo>& samples,
    const std::string& dataorder,
    const int64_t inputbinsize,
    const int64_t outputbinsize) {
  std::vector<int64_t> sortedIndices(samples.size());
  std::iota(sortedIndices.begin(), sortedIndices.end(), 0);
  if (dataorder.compare("input_spiral") == 0) {
    // Sort samples in increasing order of output bins. For samples in the same
    // output bin, sorting is done based on input size in alternating manner
    // (spiral) for consecutive bins.
    VLOG(1) << "Doing data ordering by input_spiral";
    std::sort(
        sortedIndices.begin(),
        sortedIndices.end(),
        [&](int64_t i1, int64_t i2) {
          auto& s1 = samples[i1];
          auto& s2 = samples[i2];
          auto s1_y = s1.reflength() / outputbinsize;
          auto s2_y = s2.reflength() / outputbinsize;
          if (s1_y != s2_y) {
            return s1_y < s2_y;
          }
          if (s1_y % 2 == 0) {
            return s1.audiolength() < s2.audiolength();
          } else {
            return s2.audiolength() < s1.audiolength();
          }
        });
  } else if (dataorder.compare("output_spiral") == 0) {
    // Sort samples in increasing order of input bins. For samples in the same
    // input bin, sorting is done based on output size in alternating manner
    // (spiral) for consecutive bins.
    VLOG(1) << "Doing data ordering by output_spiral";
    std::sort(
        sortedIndices.begin(),
        sortedIndices.end(),
        [&](int64_t i1, int64_t i2) {
          auto& s1 = samples[i1];
          auto& s2 = samples[i2];
          int s1_x = s1.audiolength() / inputbinsize;
          int s2_x = s2.audiolength() / inputbinsize;
          if (s1_x != s2_x) {
            return s1_x < s2_x;
          }
          if (s1_x % 2 == 0) {
            return s1.reflength() < s2.reflength();
          } else {
            return s2.reflength() < s1.reflength();
          }
        });
  } else if (dataorder.compare("input") == 0) {
    // Sort by input size
    VLOG(1) << "Doing data ordering by input";
    std::sort(
        sortedIndices.begin(),
        sortedIndices.end(),
        [&](int64_t i1, int64_t i2) {
          auto& s1 = samples[i1];
          auto& s2 = samples[i2];
          return s1.audiolength() < s2.audiolength();
        });
  } // Default is no sorting.

  std::vector<int64_t> sortedSampleIndices(samples.size());
  for (size_t i = 0; i < sortedSampleIndices.size(); ++i) {
    sortedSampleIndices[i] = samples[sortedIndices[i]].index();
  }
  return sortedSampleIndices;
}
void filterSamples(
    std::vector<SpeechSampleMetaInfo>& samples,
    const int64_t minInputSz,
    const int64_t maxInputSz,
    const int64_t minTargetSz,
    const int64_t maxTargetSz) {
  auto initialSize = samples.size();
  samples.erase(
      std::remove_if(
          samples.begin(),
          samples.end(),
          [minInputSz, maxInputSz, minTargetSz, maxTargetSz](
              const SpeechSampleMetaInfo& sample) {
            return sample.audiolength() < minInputSz ||
                sample.audiolength() > maxInputSz ||
                sample.reflength() < minTargetSz ||
                sample.reflength() > maxTargetSz;
          }),
      samples.end());
  LOG(INFO) << "Filtered " << initialSize - samples.size() << "/" << initialSize
            << " samples";
}
} // namespace w2l
