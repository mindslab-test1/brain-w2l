#include "data/Featurize.h"
#include "data/W2lOnlineLoader.h"

namespace w2l {
  W2lOnlineLoader::W2lOnlineLoader()= default;

  W2lOnlineLoader::~W2lOnlineLoader()= default;

  W2lFeatureInputData W2lOnlineLoader::load(const std::string& data, bool isRaw) {
    std::istringstream audiois(data);

    std::vector<W2lLoaderData> data1(1, W2lLoaderData());
    data1[0].input = loadSound<float>(audiois, isRaw);

    W2lFeatureInputData feat = featurizeInput(data1);
    return feat;
  }
} // namespace w2l