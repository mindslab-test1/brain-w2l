/**
 * Copyright (c) Facebook, Inc. and its affiliates.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree.
 */

#pragma once

#include <flashlight/contrib/contrib.h>
#include <flashlight/flashlight.h>

namespace w2l {

using GetConvLmScoreFunc = std::function<std::vector<std::vector<
    float>>(const std::vector<int>&, const std::vector<int>&, int, int)>;

class ConvLmModule {
public:
  explicit ConvLmModule(std::shared_ptr<fl::Module>& network);
  std::vector<std::vector<float>> score(
      const std::vector<int>& inputs,
      const std::vector<int>& lastTokenPositions,
      int sampleSize = -1,
      int batchSize = 1);
private:
  std::shared_ptr<fl::Module> network_;
  std::mutex mutex_lock_;
};

GetConvLmScoreFunc buildGetConvLmScoreFunction(
    std::shared_ptr<ConvLmModule>& convLmModule);

} // namespace w2l
