#include <flashlight/flashlight.h>
#include <glog/logging.h>

#include "criterion/criterion.h"
#include "runtime/runtime.h"

using namespace w2l;

auto tempModelPath = [](const std::string& path) { return path + ".tmp"; };

void saveToBinaryDump(
    const char* fname,
    std::shared_ptr<fl::Module> ntwrk,
    std::shared_ptr<fl::Module> crit) {
  ntwrk->eval();
  crit->eval();
  for (int i = 0; i < ntwrk->params().size(); ++i) {
    std::string key = "net-" + std::to_string(i);
    af::saveArray(key.c_str(), ntwrk->param(i).array(), fname, (i != 0));
  }

  for (int i = 0; i < crit->params().size(); ++i) {
    std::string key = "crt-" + std::to_string(i);
    af::saveArray(key.c_str(), crit->param(i).array(), fname, true);
  }
}

int main(int argc, char** argv) {
  google::InitGoogleLogging(argv[0]);
  google::InstallFailureSignalHandler();

  std::shared_ptr<fl::Module> network;
  std::shared_ptr<SequenceCriterion> criterion;
  std::unordered_map<std::string, std::string> cfg;

  if (argc < 2) {
    LOG(FATAL)
        << "Incorrect usage. 'ModelConverter [model_path]'";
  }

  std::string modelPath = argv[1];

  LOG(INFO) << "Saving params from `old binary` model to a binary dump";
  W2lSerializer::load(modelPath, cfg, network, criterion);
  saveToBinaryDump(tempModelPath(modelPath).c_str(), network, criterion);

  LOG(INFO) << "Done !";
  return 0;
}