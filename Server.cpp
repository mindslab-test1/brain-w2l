#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <mutex>
#include <string>
#include <vector>
#include <regex>

#include <flashlight/flashlight.h>
#include <gflags/gflags.h>
#include <glog/logging.h>

#include "common/Defines.h"
#include "common/FlashlightUtils.h"
#include "criterion/criterion.h"
#include "data/Featurize.h"
#include "data/Utils.h"
#include "data/W2lOnlineLoader.h"
#include "libraries/common/Dictionary.h"
#include "libraries/decoder/LexiconFreeDecoder.h"
#include "libraries/decoder/Seq2SeqDecoder.h"
#include "libraries/decoder/TokenLMDecoder.h"
#include "libraries/decoder/WordLMDecoder.h"
#include "libraries/lm/ConvLM.h"
#include "libraries/lm/KenLM.h"
#include "libraries/lm/ZeroLM.h"
#include "module/module.h"
#include "runtime/runtime.h"

#include <grpcpp/server.h>
#include <grpcpp/server_builder.h>
#include <grpcpp/server_context.h>
#include <grpcpp/security/server_credentials.h>
#include <grpcpp/create_channel.h>
#include "maum/brain/w2l/w2l.grpc.pb.h"
#include "maum/brain/w2l/w2l.pb.h"
#include "maum/brain/vad/vad.grpc.pb.h"

using namespace w2l;

using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::ServerReader;
using grpc::ServerReaderWriter;
using grpc::Status;
using grpc::StatusCode;
using grpc::ClientContext;
using grpc::ClientReaderWriter;
using google::protobuf::Empty;
using maum::brain::w2l::SpeechToText;
using maum::brain::w2l::Speech;
using maum::brain::w2l::Text;
using maum::brain::w2l::TextSegment;
using maum::brain::w2l::PosteriorGram;
using maum::brain::w2l::PosteriorGramInfo;
using maum::brain::vad::Vad;
using maum::brain::vad::Segment;


class DecoderPool {
public:
  explicit DecoderPool(std::shared_ptr<SequenceCriterion>& criterion, LexiconMap& lexicon,
                       Dictionary& wordDict, Dictionary& tokenDict) {
    std::vector<float> transition;
    if (FLAGS_criterion == kAsgCriterion) {
      transition = afToVector<float>(criterion->param(0).array());
    }

    /* ===================== Decode ===================== */
    // Prepare criterion
    CriterionType criterionType = CriterionType::ASG;
    if (FLAGS_criterion == kCtcCriterion) {
      criterionType = CriterionType::CTC;
    } else if (FLAGS_criterion == kSeq2SeqCriterion) {
      criterionType = CriterionType::S2S;
    } else if (FLAGS_criterion != kAsgCriterion) {
      LOG(FATAL) << "[Decoder] Invalid model type: " << FLAGS_criterion;
    }

    // Prepare decoder options
    DecoderOptions decoderOpt(
        FLAGS_beamsize,
        static_cast<float>(FLAGS_beamthreshold),
        static_cast<float>(FLAGS_lmweight),
        static_cast<float>(FLAGS_wordscore),
        static_cast<float>(FLAGS_unkweight),
        FLAGS_logadd,
        static_cast<float>(FLAGS_silweight),
        criterionType);

    // Build Language Model
    int unkWordIdx = -1;

    Dictionary usrDict = tokenDict;
    if (!FLAGS_lm.empty() && FLAGS_decodertype == "wrd") {
      usrDict = wordDict;
      unkWordIdx = wordDict.getIndex(kUnkToken);
    }

    std::shared_ptr<LM> lm = std::make_shared<ZeroLM>();
    GetConvLmScoreFunc getConvLmScoreFunc;
    if (!FLAGS_lm.empty()) {
      if (FLAGS_lmtype == "kenlm") {
        lm = std::make_shared<KenLM>(FLAGS_lm, usrDict);
        if (!lm) {
          LOG(FATAL) << "[LM constructing] Failed to load LM: " << FLAGS_lm;
        }
      } else if (FLAGS_lmtype == "convlm") {
        af::setDevice(0);
        LOG(INFO) << "[ConvLM]: Loading LM from " << FLAGS_lm;
        std::shared_ptr<fl::Module> convLmModel;
        W2lSerializer::load(FLAGS_lm, convLmModel);
        convLmModel->eval();

        std::shared_ptr<ConvLmModule> convLmModule = std::make_shared<ConvLmModule>(convLmModel);
        getConvLmScoreFunc = buildGetConvLmScoreFunction(convLmModule);
        lm = std::make_shared<ConvLM>(
            getConvLmScoreFunc,
            FLAGS_lm_vocab,
            usrDict,
            FLAGS_lm_memory,
            FLAGS_beamsize);
      } else {
        LOG(FATAL) << "[LM constructing] Invalid LM Type: " << FLAGS_lmtype;
      }
    }
    LOG(INFO) << "[Decoder] LM constructed.\n";

    // Build Trie
    int blankIdx =
        FLAGS_criterion == kCtcCriterion ? tokenDict.getIndex(kBlankToken) : -1;
    int silIdx = tokenDict.getIndex(FLAGS_wordseparator);
    std::shared_ptr<Trie> trie = nullptr;
    if (FLAGS_uselexicon) {
      trie = std::make_shared<Trie>(tokenDict.indexSize(), silIdx);
      auto startState = lm->start(false);

      for (auto& it : lexicon) {
        const std::string& word = it.first;
        int usrIdx = wordDict.getIndex(word);
        float score = -1;
        if (FLAGS_decodertype == "wrd") {
          LMStatePtr dummyState;
          std::tie(dummyState, score) = lm->score(startState, usrIdx);
        }
        for (auto& tokens : it.second) {
          auto tokensTensor = tkn2Idx(tokens, tokenDict, FLAGS_replabel);
          trie->insert(tokensTensor, usrIdx, score);
        }
      }
      LOG(INFO) << "[Decoder] Trie planted.\n";

      // Smearing
      SmearingMode smear_mode = SmearingMode::NONE;
      if (FLAGS_smearing == "logadd") {
        smear_mode = SmearingMode::LOGADD;
      } else if (FLAGS_smearing == "max") {
        smear_mode = SmearingMode::MAX;
      } else if (FLAGS_smearing != "none") {
        LOG(FATAL) << "[Decoder] Invalid smearing mode: " << FLAGS_smearing;
      }
      trie->smear(smear_mode);
      LOG(INFO) << "[Decoder] Trie smeared.\n";
    }

    if (criterionType == CriterionType::S2S) {
      FLAGS_nthread_decoder = 1;
    }
    for (int i = 0; i < FLAGS_nthread_decoder; ++i) {
      if (FLAGS_lmtype == "convlm") {
        lm = std::make_shared<ConvLM>(
            getConvLmScoreFunc,
            FLAGS_lm_vocab,
            usrDict,
            FLAGS_lm_memory,
            FLAGS_beamsize);
      }
      // Build Decoder
      std::shared_ptr<Decoder> decoder;
      if (FLAGS_decodertype == "wrd") {
        decoder = std::make_shared<WordLMDecoder>(
            decoderOpt,
            trie,
            lm,
            silIdx,
            blankIdx,
            unkWordIdx,
            transition);
        if (i == 0)
          LOG(INFO) << "[Decoder] Decoder with word-LM loaded";
      } else if (FLAGS_decodertype == "tkn") {
        if (criterionType == CriterionType::S2S) {
          auto amUpdateFunc = buildAmUpdateFunction(criterion);
          int eosIdx = tokenDict.getIndex(kEosToken);

          decoder = std::make_shared<Seq2SeqDecoder>(
              decoderOpt,
              lm,
              eosIdx,
              amUpdateFunc,
              FLAGS_maxdecoderoutputlen,
              static_cast<float>(FLAGS_hardselection),
              static_cast<float>(FLAGS_softselection));
          if (i == 0)
            LOG(INFO) << "[Decoder] Seq2Seq decoder with token-LM loaded";
        } else if (FLAGS_uselexicon) {
          decoder = std::make_shared<TokenLMDecoder>(
              decoderOpt,
              trie,
              lm,
              silIdx,
              blankIdx,
              unkWordIdx,
              transition);
          if (i == 0)
            LOG(INFO) << "[Decoder] Decoder with token-LM loaded";
        } else {
          decoder = std::make_shared<LexiconFreeDecoder>(
              decoderOpt, lm, silIdx, blankIdx, transition);
          if (i == 0)
            LOG(INFO) << "[Decoder] Lexicon-free decoder with token-LM loaded";
        }
      } else {
        if (i == 0)
          LOG(FATAL) << "Unsupported decoder type: " << FLAGS_decodertype;
      }
      decoders_.emplace_back(decoder);
    }
  }
  std::shared_ptr<Decoder> pop() {
    while (true) {
      {
        std::lock_guard<std::mutex> guard(mutex_lock_);
        if (!decoders_.empty()) {
          auto decoder = decoders_.back();
          decoders_.pop_back();
          return decoder;
        }
      }
      usleep(100);
    }
  }
  void push(std::shared_ptr<Decoder>& decoder) {
    std::lock_guard<std::mutex> guard(mutex_lock_);
    decoders_.emplace_back(decoder);
  }

private:
  std::vector<std::shared_ptr<Decoder>> decoders_;
  std::mutex mutex_lock_;
};

struct Emission {
  std::vector<float> data;
  int N = 0;
  int T = 0;
  double duration = 0;
};

struct NetworkInput {
  W2lFeatureInputData feature;
  std::chrono::steady_clock::time_point order;
  std::shared_ptr<std::promise<Emission>> p;
};

bool operator<(const NetworkInput& networkInput1, const NetworkInput& networkInput2) {
  return networkInput1.order > networkInput2.order;
}

class NetworkThread {
public:
  explicit NetworkThread(std::shared_ptr<fl::Module>& network) : network_(network), stop_(false) {
    thread_ = std::thread([this]() {
      af::setDevice(0);
      fl::MemoryManagerInstaller::installDefaultMemoryManager();
      warmup_();
      while (true) {
        NetworkInput networkInput;
        {
          std::unique_lock<std::mutex> guard(this->mutex_lock_);
          this->condition_.wait(guard, [this]{ return this->stop_ || !this->priorityQueue_.empty(); });
          if (this->stop_ && this->priorityQueue_.empty())
            return;
          networkInput = this->priorityQueue_.top();
          this->priorityQueue_.pop();
          VLOG(1) << "nn priority queue size:" << this->priorityQueue_.size();
        }
        std::chrono::steady_clock::time_point start;
        if (VLOG_IS_ON(1)) {
          start = std::chrono::steady_clock::now();
        }
        af::array feature = makeFeature_(networkInput.feature);
        fl::Variable rawEmission;
        if (feature.dims(0) < FLAGS_maxchunkframe) {
          rawEmission = this->network_->forward({fl::input(feature)}).front();
        } else {
          std::vector <fl::Variable> rawEmissions;
          for (int start = 0; start < (int) feature.dims(0); start += FLAGS_maxchunkframe) {
            int end = std::min(start + FLAGS_maxchunkframe, (int) feature.dims(0)) - 1;
            af::array featureChunk = feature.rows(start, end);
            auto rawEmissionChunk = this->network_->forward({fl::input(featureChunk)}).front();
            rawEmissions.emplace_back(rawEmissionChunk);
          }
          rawEmission = fl::concatenate(rawEmissions, 1);
        }

        int N = rawEmission.dims(0);
        int T = rawEmission.dims(1);

        auto emission = afToVector<float>(rawEmission);
        Emission ret;
        ret.data = emission;
        ret.N = N;
        ret.T = T;
        if (VLOG_IS_ON(1)) {
          std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
          std::chrono::duration<double> duration = end - start;
          ret.duration = duration.count();
        }
        networkInput.p->set_value(ret);
      }
    });
  };

  std::future<Emission> enqueue(NetworkInput& networkInput) {
    networkInput.p = std::make_shared<std::promise<Emission>>();
    std::future<Emission> res = networkInput.p->get_future();
    {
      std::unique_lock<std::mutex> guard(mutex_lock_);
      if(stop_)
        throw std::runtime_error("enqueue on stopped ThreadPool");
      priorityQueue_.emplace(networkInput);
    }
    condition_.notify_one();
    return res;
  };

  ~NetworkThread() {
    {
      std::unique_lock<std::mutex> lock(mutex_lock_);
      stop_ = true;
    }
    condition_.notify_all();
    thread_.join();
  }

private:
  std::shared_ptr<fl::Module> network_;
  std::mutex mutex_lock_;
  std::priority_queue<NetworkInput> priorityQueue_;
  std::condition_variable condition_;
  std::thread thread_;
  bool stop_;

  void warmup_() {
    long maxAudioLength = FLAGS_samplerate * FLAGS_maxchunkframe * FLAGS_framestridems / 1000;
    std::vector<float> audio(maxAudioLength);

    std::vector<W2lLoaderData> data1(1, W2lLoaderData());
    data1[0].input = audio;

    W2lFeatureInputData feat = featurizeInput(data1);

    af::array feature = makeFeature_(feat);
    this->network_->forward({fl::input(feature)}).front();
  }

  af::array makeFeature_(W2lFeatureInputData& feat) {
    auto inputDims = af::dim4(
        feat.inputDims[0],
        feat.inputDims[1],
        feat.inputDims[2],
        feat.inputDims[3]
    );
    af::array feature = af::array(inputDims, feat.data.data());
    return feature;
  }
};

struct SpeechPosition {
  unsigned long audioStart;
  unsigned long audioEnd;
  int segmentStart;
  int segmentEnd;
};

class SpeechQueue {
public:
  void push(const std::string& data) {
    std::lock_guard<std::mutex> guard(mutex_lock_);
    data_.append(data);
  }
  std::string pop(int start, int end) {
    std::lock_guard<std::mutex> guard(mutex_lock_);
    int speechLength = end - start;
    std::string returnData = data_.substr(start - offset_, speechLength);
    data_.erase(0, end - offset_);
    offset_ = end;
    return returnData;
  }

private:
  std::string data_;
  int offset_ = 0;
  std::mutex mutex_lock_;
};


class SpeechToTextImpl final : public SpeechToText::Service {
public:
  explicit SpeechToTextImpl(int argc, char** argv, const std::string& flagsfile) {
    /* ===================== Create Network ===================== */
    std::shared_ptr<SequenceCriterion> criterion;
    std::unordered_map<std::string, std::string> cfg;

    /* Using acoustic model */
    LOG(INFO) << "[Network] Reading acoustic model from " << FLAGS_am;
    af::setDevice(0);
    W2lSerializer::load(FLAGS_am, cfg, network_, criterion);
    network_->eval();
    LOG(INFO) << "[Network] " << network_->prettyString();
    if (criterion) {
      criterion->eval();
      LOG(INFO) << "[Criterion] " << criterion->prettyString();
    }
    LOG(INFO) << "[Network] Number of params: " << numTotalParams(network_);

    auto flags = cfg.find(kGflags);
    if (flags == cfg.end()) {
      LOG(FATAL) << "[Network] Invalid config loaded from " << FLAGS_am;
    }
    LOG(INFO) << "[Network] Updating flags from config file: " << FLAGS_am;
    gflags::ReadFlagsFromString(flags->second, gflags::GetArgv0(), true);

    // override with user-specified flags
    gflags::ParseCommandLineFlags(&argc, &argv, false);
    if (!flagsfile.empty()) {
      gflags::ReadFromFlagsFile(flagsfile, argv[0], true);
      // Re-parse command line flags to override values in the flag file.
      gflags::ParseCommandLineFlags(&argc, &argv, false);
    }

    LOG(INFO) << "Gflags after parsing \n" << serializeGflags("; ");

    /* ===================== Create Dictionary ===================== */
    auto dictPath = pathsConcat(FLAGS_tokensdir, FLAGS_tokens);
    if (dictPath.empty() || !fileExists(dictPath)) {
      throw std::runtime_error("Invalid dictionary filepath specified.");
    }
    tokenDict_ = Dictionary(dictPath);
    // Setup-specific modifications
    for (int64_t r = 1; r <= FLAGS_replabel; ++r) {
      tokenDict_.addEntry(std::to_string(r));
    }
    // ctc expects the blank label last
    if (FLAGS_criterion == kCtcCriterion) {
      tokenDict_.addEntry(kBlankToken);
    }
    if (FLAGS_eostoken) {
      tokenDict_.addEntry(kEosToken);
    }

    int numClasses = tokenDict_.indexSize();
    LOG(INFO) << "Number of classes (network): " << numClasses;

    LexiconMap lexicon;
    if (!FLAGS_lexicon.empty()) {
      lexicon = loadWords(FLAGS_lexicon, FLAGS_maxword);
      wordDict_ = createWordDict(lexicon);
      LOG(INFO) << "Number of words: " << wordDict_.indexSize();
    }

    /* ===================== Create Data Loader ===================== */
    // Load data loader
    int worldRank = 0;
    int worldSize = 1;
    dataLoader_ = std::make_shared<W2lOnlineLoader>();

    /* ===================== Create Pool ==================== */
    lmPool_ = std::make_shared<DecoderPool>(criterion, lexicon, wordDict_, tokenDict_);
    networkThread_ = std::make_shared<NetworkThread>(network_);

    /* ===================== Setup Detail Segment ==================== */
    useDetailSegment_ = (FLAGS_eostoken || FLAGS_usedetailsegment) && FLAGS_separatedvadresult;
    if (tokenDict_.contains(kEosToken) && FLAGS_eostoken) {
      eosIdx_ = tokenDict_.getIndex(kEosToken);
    }
    blankIdx_ = tokenDict_.getIndex(FLAGS_wordseparator);
    if (tokenDict_.contains("#") && FLAGS_usemecab) {
      mecabIdx_ = tokenDict_.getIndex("#");
    }

    frameSize_ = FLAGS_framestridems * (FLAGS_samplerate / 2000);
    frameOffset_ = FLAGS_framesizems * (FLAGS_samplerate / 1000);
  }

  Status GetPosteriorGramInfo(ServerContext* context, const Empty* empty, PosteriorGramInfo* posteriorGramInfo) override {
    try {
      for (size_t charIdx = 0 ; charIdx < tokenDict_.indexSize(); charIdx++) {
        std::string token = tokenDict_.getEntry(charIdx);
        posteriorGramInfo->add_tokens(token);
      }
      return Status::OK;
    } catch (const std::exception& exc) {
      LOG(ERROR) << "Error while writing logs: " << exc.what();
      return Status(StatusCode::UNKNOWN, exc.what());
    }
  }

  Status ComputePosteriorGram(ServerContext* context, ServerReader<Speech>* reader, PosteriorGram* posteriorGram) override {
    std::chrono::steady_clock::time_point now = std::chrono::steady_clock::now();
    try {
      Speech speech;
      std::string audio;

      while (reader->Read(&speech)) {
        audio.append(speech.bin());
      }

      auto emission = runNetwork(audio, now);

      posteriorGram->set_n(emission.N);
      posteriorGram->set_t(emission.T);
      for (auto data : emission.data) {
        posteriorGram->add_logit(data);
      }

      return Status::OK;
    } catch (const std::exception& exc) {
      LOG(ERROR) << "Error while writing logs: " << exc.what();
      return Status(StatusCode::UNKNOWN, exc.what());
    }
  }

  Status Recognize(ServerContext* context, ServerReader<Speech>* reader, Text* text) override {
    std::chrono::steady_clock::time_point now = std::chrono::steady_clock::now();
    try {
      Speech speech;
      std::string audio;

      while (reader->Read(&speech)) {
        audio.append(speech.bin());
      }

      auto emission = runNetwork(audio, now);

      auto decoder = lmPool_->pop();
      std::vector<DecodeResult> results;
      try {
        results = decoder->decode(emission.data.data(), emission.T, emission.N);
      } catch (const std::exception& exc) {
        lmPool_->push(decoder);
        throw;
      }
      lmPool_->push(decoder);

      std::string txt = cleanupPrediction(results[0]);
      text->set_txt(txt);
      VLOG(2) << "output text: " << txt;
      if (VLOG_IS_ON(1)) {
        std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
        std::chrono::duration<double> duration = end - now;
        VLOG(1) << "total runtime(s): " << duration.count();
      }
      return Status::OK;
    } catch (const std::exception& exc) {
      LOG(ERROR) << "Error while writing logs: " << exc.what();
      return Status(StatusCode::UNKNOWN, exc.what());
    }
  }

  Status StreamRecognize(ServerContext *context, ServerReaderWriter<TextSegment, Speech> *stream) override {
    std::chrono::steady_clock::time_point now = std::chrono::steady_clock::now();
    unsigned long speechLength = 0;
    unsigned long activeLength = 0;
    try {
      std::shared_ptr<Vad::Stub> vadStub = Vad::NewStub(
          grpc::CreateChannel(
              FLAGS_vadremote,
              grpc::InsecureChannelCredentials()
          )
      );

      ClientContext clientContext;
      std::shared_ptr<ClientReaderWriter<maum::brain::vad::Speech, Segment>> vadStream(vadStub->Detect(&clientContext));

      SpeechQueue speechQueue;
      auto asyncWriter = std::async(std::launch::async, [vadStream, stream, &speechQueue, &speechLength, &activeLength]() {
        Speech speech;
        maum::brain::vad::Speech vadSpeech;
        while (stream->Read(&speech)) {
          speechQueue.push(speech.bin());
          speechLength += speech.bin().size();
          vadSpeech.set_bin(speech.bin());
          vadStream->Write(vadSpeech);
        }
        vadStream->WritesDone();
      });

      Segment segment;
      auto decoder = lmPool_->pop();
      try {
        decoder->decodeBegin();
        std::vector<SpeechPosition> speechPositions;
        std::string audios;
        while (vadStream->Read(&segment)) {
          VLOG(2) << "vad start: " << segment.start() << ", end: " << segment.end();
          std::string audio = speechQueue.pop(segment.start() * 2, segment.end() * 2);
          activeLength += audio.size();
          SpeechPosition speechPosition = {
              audios.size(), audios.size() + audio.size(),
              segment.start(), segment.end()
          };
          speechPositions.emplace_back(speechPosition);
          audios.append(audio);
          if (FLAGS_minchunkaudio < audios.size()) {
            streamRecognize(audios, decoder, speechPositions, stream, now);
            speechPositions.clear();
            audios.clear();
          }
        }
        if (!audios.empty()) {
          streamRecognize(audios, decoder, speechPositions, stream, now);
        }
      } catch (const std::exception& exc) {
        decoder->decodeEnd();
        lmPool_->push(decoder);
        throw;
      }
      decoder->decodeEnd();
      lmPool_->push(decoder);
      asyncWriter.wait();
      Status vadStatus = vadStream->Finish();
      if (!vadStatus.ok()) {
        std::string errMsg = vadStatus.error_message();
        LOG(ERROR) << "VAD Detect rpc error msg:" << errMsg;
        return Status(StatusCode::UNKNOWN, "vad error");
      }
      if (VLOG_IS_ON(1)) {
        std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
        std::chrono::duration<double> duration = end - now;
        VLOG(1) << "total runtime(s): " << duration.count() << ", active rate: " << (double) activeLength / speechLength;
      }
      return Status::OK;
    } catch (const std::exception& exc) {
      LOG(ERROR) << "Error while writing logs: " << exc.what();
      return Status(StatusCode::UNKNOWN, exc.what());
    }
  }

private:
  std::shared_ptr<fl::Module> network_;
  std::shared_ptr<W2lOnlineLoader> dataLoader_;
  Dictionary wordDict_;
  Dictionary tokenDict_;
  std::shared_ptr<DecoderPool> lmPool_;
  std::shared_ptr<NetworkThread> networkThread_;
  int eosIdx_ = -1;
  int blankIdx_ = -1;
  int mecabIdx_ = -1;

  long frameSize_;
  long frameOffset_;

  bool useDetailSegment_;

  void streamRecognize(std::string& audio, std::shared_ptr<Decoder>& decoder,
      std::vector<SpeechPosition>& speechPositions, ServerReaderWriter<TextSegment, Speech> *stream,
      std::chrono::steady_clock::time_point& now) {
    auto firstSpeechPosition = speechPositions.front();
    int offsetMs = 1000 * firstSpeechPosition.segmentStart / (int)FLAGS_samplerate;
    auto order = now + std::chrono::milliseconds(offsetMs);
    auto emission = runNetwork(audio, order, true);
    std::chrono::steady_clock::time_point start;
    if (VLOG_IS_ON(1)) {
      start = std::chrono::steady_clock::now();
    }
    int subStart = 0;
    for (auto& speechPosition : speechPositions) {
      int subEnd = 1 + (int)floor(((double)speechPosition.audioEnd / 2.0 - (double)frameOffset_) / (double)frameSize_ / 2.0);

      Emission subEmission;
      subEmission.N = emission.N;
      subEmission.T = subEnd - subStart;
      subEmission.data = std::vector<float>(
          emission.data.begin() + emission.N * subStart,
          emission.data.begin() + emission.N * subEnd);

      decoder->decodeStep(subEmission.data.data(), subEmission.T, subEmission.N);
      DecodeResult result = decoder->getBestHypothesis();
      decoder->prune();

      subStart = subEnd;
      if (useDetailSegment_) {
        unsigned long segmentStart = speechPosition.segmentStart;

        bool isSkip = true;
        bool isBlankStart = false;
        bool isEosStart = false;
        unsigned long eosStart = 0;
        unsigned long textStart = 0;
        for (unsigned long position = 0; position < result.tokens.size(); position++) {
          auto token = result.tokens[position];
          if (isSkip) {
            if (token == eosIdx_ || token == blankIdx_) {
              continue;
            } else {
              isSkip = false;
              segmentStart = speechPosition.segmentStart + 2 * position * frameSize_ - frameOffset_;
              if (segmentStart < speechPosition.segmentStart) {
                segmentStart = speechPosition.segmentStart;
              }
            }
          }
          if (!isBlankStart && ( token == eosIdx_ || token == blankIdx_ ) ) {
            isBlankStart = true;
            isEosStart = (token == eosIdx_);
            eosStart = position;
          } else if (isBlankStart && !isEosStart && token == eosIdx_ ) {
            isEosStart = true;
          } else if (isBlankStart && token == mecabIdx_) {
            isBlankStart = false;
            isEosStart = false;
          } else if (isBlankStart && token != eosIdx_ && token != blankIdx_ && (FLAGS_usedetailsegment || isEosStart)) {
            isBlankStart = false;
            isEosStart = false;
            unsigned long segmentEnd = speechPosition.segmentStart + (eosStart + position) * frameSize_ + frameOffset_;
            if (speechPosition.segmentEnd < segmentEnd) {
              segmentEnd = speechPosition.segmentEnd;
            }

            DecodeResult resultSegment;
            resultSegment.tokens = std::vector<int>(
                result.tokens.begin()+textStart, result.tokens.begin()+position);
            resultSegment.words = std::vector<int>(
                result.words.begin()+textStart, result.words.begin()+position);
            writeSegment(resultSegment, segmentStart, segmentEnd, stream);

            segmentStart = speechPosition.segmentStart + 2 * position * frameSize_ - frameOffset_;
            if (segmentStart < segmentEnd) {
              segmentStart = segmentEnd;
            }
            textStart = position;
          }
        }
        DecodeResult resultSegment;
        resultSegment.tokens = std::vector<int>(
            result.tokens.begin()+textStart, result.tokens.end());
        resultSegment.words = std::vector<int>(
            result.words.begin()+textStart, result.words.end());

        writeSegment(resultSegment, segmentStart, speechPosition.segmentEnd, stream);
      } else {
        writeSegment(result, speechPosition.segmentStart, speechPosition.segmentEnd, stream);
      }
    }
    if (VLOG_IS_ON(1)) {
      std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
      std::chrono::duration<double> duration = end - start;
      VLOG(1) << "lm runtime(s): " << duration.count();
    }
  }
  void writeSegment(DecodeResult& result, unsigned long segmentStart, unsigned long segmentEnd,
      ServerReaderWriter<TextSegment, Speech>* stream) {
    std::string txt = cleanupPrediction(result);
    if (!txt.empty()) {
      TextSegment textSegment;
      textSegment.set_txt(txt);
      textSegment.set_start(segmentStart);
      textSegment.set_end(segmentEnd);

      stream->Write(textSegment);
      VLOG(2) << "(start, end) = (" << segmentStart << ", " << segmentEnd << "), output text: " << txt;
    }
  }
  Emission runNetwork(
      std::string& audio, std::chrono::steady_clock::time_point& order, bool isRaw=false) {
    VLOG(2) << "order: " << order.time_since_epoch().count();
    VLOG(2) << "input size: " << audio.size();
    W2lFeatureInputData feature = dataLoader_->load(audio, isRaw);

    VLOG(2) << "feature size(N, T): " << feature.inputDims[1] << ", " << feature.inputDims[0];

    NetworkInput networkInput;
    networkInput.feature = feature;
    networkInput.order = order;
    std::chrono::steady_clock::time_point start;
    if (VLOG_IS_ON(1)) {
      start = std::chrono::steady_clock::now();
    }
    auto result = networkThread_->enqueue(networkInput);
    auto ret = result.get();
    if (VLOG_IS_ON(1)) {
      std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
      std::chrono::duration<double> duration = end - start;
      double waitTime = duration.count() - ret.duration;
      VLOG(1) << "wait runtime(s): " << waitTime << ", am runtime(s): " << ret.duration;
    }
    VLOG(2) << "emission size(N, T): " << ret.N << ", " << ret.T;
    return ret;
  }
  std::string cleanupPrediction(DecodeResult& result) {
    auto& rawWordPrediction = result.words;
    auto& rawTokenPrediction = result.tokens;

    std::vector<std::string> wordPrediction;
    if (FLAGS_uselexicon) {
      rawWordPrediction = validateIdx(rawWordPrediction, wordDict_.getIndex(kUnkToken));
      wordPrediction = wrdIdx2Wrd(rawWordPrediction, wordDict_);
    } else {
      auto letterPrediction = tknPrediction2Ltr(rawTokenPrediction, tokenDict_);
      wordPrediction = tkn2Wrd(letterPrediction);
    }

    auto wordPredictionStr = join(" ", wordPrediction);
    if (FLAGS_usemecab) {
      wordPredictionStr = std::regex_replace(wordPredictionStr, std::regex(" *#"), "");
    }
    if (FLAGS_usenormalizer) {
      wordPredictionStr = nfkc(wordPredictionStr);
    }
    if (FLAGS_eostoken) {
      wordPredictionStr = std::regex_replace(wordPredictionStr, std::regex(" *[$]+ *"), "\n");
      wordPredictionStr = std::regex_replace(wordPredictionStr, std::regex("^ +| +$|^\n$"), "");
    }
    return wordPredictionStr;
  }
};

int main(int argc, char** argv) {
  google::InitGoogleLogging(argv[0]);
  google::InstallFailureSignalHandler();
  std::string exec(argv[0]);
  std::vector<std::string> argvs;
  for (int i = 0; i < argc; i++) {
    argvs.emplace_back(argv[i]);
  }
  gflags::SetUsageMessage("Usage: Please refer to https://git.io/fjVVq");
  if (argc <= 1) {
    LOG(FATAL) << gflags::ProgramUsage();
  }

  /* ===================== Parse Options ===================== */
  LOG(INFO) << "Parsing command line flags";
  gflags::ParseCommandLineFlags(&argc, &argv, false);
  auto flagsfile = FLAGS_flagsfile;
  if (!flagsfile.empty()) {
    LOG(INFO) << "Reading flags from file " << flagsfile;
    gflags::ReadFromFlagsFile(flagsfile, argv[0], true);
    // Re-parse command line flags to override values in the flag file.
    gflags::ParseCommandLineFlags(&argc, &argv, false);
  }

  /* ===================== Make gRPC Server ===================== */
  std::string server_address("0.0.0.0:");
  server_address += std::to_string(FLAGS_port);
  SpeechToTextImpl service(argc, argv, flagsfile);

  ServerBuilder builder;
  builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
  builder.RegisterService(&service);
  std::unique_ptr<Server> server(builder.BuildAndStart());
  LOG(INFO) << "Server listening on " << server_address << std::endl;
  server->Wait();

  return 0;
}