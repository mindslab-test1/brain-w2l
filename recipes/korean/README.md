## data
- 음향모델(Acoustic Model, AM)과 언어 모델(Language Model, LM)을 돌리기 위해서 raw data를 preprocessing 하는 코드.

## test
- 모델의 성능을 Letter Error Rate로 gRPC나 result 파일로 측정할 수 있는 코드.

## network.arch
- 음향모델(Acoustic Model, AM)의 network architecture를 정의하는 파일. baseline model를 만들 때에만 사용. 현재는 conv_glu이고 추후에 추가될 예정.

## server.cfg
- 실행 서버를 띄우기 위해 필요한 설정 파일.
- parameter
  - `tokensdir`: `tokens.txt` 파일이 있는 디렉토리의 경로
  - `lexicon`: lexicon 파일의 경로
  - `lm`: 언어 모델 경로
  - `decodertype`: 언어 모델의 타입. wrd: 단어, tkn: 글자
  - `uselexicon`: lexicon 사용 여부. word lm이면 true, token lm이면 true와 false 둘다 가능
  - `am`: 음향 모델 경로
  - `nthread_decoder`: 서버에 존재하는 core 갯수
  - `usemecab`: 한국어 모델일 때 true, 영어 모델이면 false
  - `usenormalizer`: 한국어 모델일 때 True, 영어 모델이면 False
  - `v`: 디버그할 때 1, 일반적으로 0
  - `vadremote`: StreamRecognize에서 사용하는, vad gRPC server의 ip:port 값.
  - `samplerate`: 샘플링레이트
  - `usedetailsegment`: StreamRecognize에서 어절 단위로 출력되게 할 때 true

## train.cfg
- 음향모델(Acoustic Model, AM)을 학습할 때 필요한 설정 파일.
- parameter
  - `train`: data set을 여러 개 사용하고 싶은 경우 array 형식으로 입력. 예를 들어, `--train=train1.lst, train2.lst` 이렇게 입력하면 됨.
  - `valid`: data set을 여러 개 사용하고 싶은 경우 map 형식으로 입력. 예를 들어, `--valid=dev1:dev1.lst,dev2:dev2.lst` 이렇게 입력하면 됨.
  - `noise`: noise data augmentation을 할 경우, noise path의 목록이 있는 lst 파일의 경로를 array 형식으로 입력. 
  - `batchsize`: 그래픽카드의 RAM 용량이 11GB일 때 문제가 없게 설정. RAM 용량이 다른 경우에 적절히 수정하면 됨.
  - `linseg`: baseline을 학습할 때에는 1, 그외에는 모두 0으로 사용.
  - `runname`: 모델명. `rundir` 안에 새로운 디렉토리로 생성.
  - `usenormalizer`: 한국어 모델일 때 True, 영어 모델이면 False. True로 하면 lexicon 불필요.
  - `usemecab`: 한국어 모델일 때 True, 영어 모델이면 False.
  - `samplerate`: 샘플링레이트
  - `eostoken`: eos(end of sentence) token을 사용하면 True.
  - `minnoisesample`: noise를 넣어서 text가 나오지 않게 하는 학습을 할 때, 샘플 길이의 최소값. (샘플링레이트/40)보다 작으면 에러 발생.
  - `maxnoisesample`: noise를 넣어서 text가 나오지 않게 하는 학습을 할 때, 샘플 길이의 최대값. (샘플링레이트)*(gpu에서 돌릴 수 있는 음성 중에서 가장 긴 시간(초)). v100, 8k기준 5280000 
---------------------------------------
## 음향 모델 학습 과정 예시
### 가정
1. 음성 데이터와 음성을 전사한 txt 파일을 동일한 이름으로 `/DATA/w2l/test/data` 안에 있음.
1. `wav2letter:cuda-latest` 이미지로 컨테이너를 띄울 때, `-v /DATA:/DATA` 이렇게 volume을 mount.
1. baseline 모델이 `/DATA/w2l/baseline/011_model_last.bin`로 있다고 가정.
### 과정
1. `wav2letter:cuda-latest` 이미지로 띄운 도커 컨테이너에 접속.
1. `cd /root/wav2letter/recipes/korean/data` 실행.
1. `python prepare_am.py --train /DATA/w2l/test/data --output /DATA/w2l/test` 실행.
1. `train.cfg`의 `[DATA_DST]`를 `/DATA/w2l/test`로 수정하여 `/DATA/w2l/test/train.cfg`를 생성.
1. (optional) baseline 모델을 학습할 경우, `network.arch`를 복사하여 `/DATA/w2l/test/network.arch`를 생성.
1. `cd /root/wav2letter/build/` 실행.
1. 학습 실행
  - 베이스라인 학습: `./Train train --logtostderr 1 -enable_distributed false --flagsfile /DATA/w2l/test/train.cfg`
  - 추가 학습: `./Train fork /DATA/w2l/baseline/011_model_last.bin -enable_distributed false --logtostderr 1 --flagsfile /DATA/w2l/test/train.cfg`
  - 이어서 학습: `./Train continue /DATA/w2l/test/model/kor_conv_glu -enable_distributed false --logtostderr 1 --flagsfile /DATA/w2l/test/train.cfg`
  - 분산 학습(8 GPU 일 때): 위의 3가지 명령어에서 `./Train`를 `mpirun -n 8 --allow-run-as-root Train`로 변경, `-enable_distributed false`를 `-enable_distributed true`로 변경.
### 학습 결과 파일 구조
- `train.cfg`의 `rundir/runname`에 학습 결과가 기록. 위의 예시에서는 `/DATA/w2l/test/model/kor_conv_glu`
```
.
├── 001_config          : 설정 파일
├── 001_log             : |key: value| 형태의 log
├── 001_model_dev.bin   : valid 데이터셋마다 성능이 가장 좋은 모델. 이 경우는 dev 셋의 경우.
├── 001_model_last.bin  : 가장 마지막으로 학습된 모델, 이어서하면 이 모델에서 이어서 학습을 함.
└── 001_perf            : table 형태의 log, header가 존재하고 data가 tab으로 구분
```
---------------------------------------
## 언어 모델 학습 과정 예시(kenlm)
### 가정
1. 언어 모델 학습 데이터가 `/DATA/w2l/test/lm/data` 안에 있음.
1. `wav2letter:cuda-latest` 이미지로 컨테이너를 띄울 때, `-v /DATA:/DATA` 이렇게 volume을 mount.
### 과정
1. `wav2letter:cuda-latest` 이미지로 띄운 도커 컨테이너에 접속.
1. `cd /root/wav2letter/recipes/korean/data` 실행.
1. `python make_lm_text.py --txt_path /DATA/w2l/test/lm/data --output /DATA/w2l/test/lm` 실행. token lm이면 `-ut` 추가
1. `/root/kenlm/build/bin/lmplz --discount_fallback -o 4 </DATA/w2l/test/lm/lm_train.txt >/DATA/w2l/test/lm/lm.arpa` 실행. token lm이면 `--discount_fallback -o 4`를 `-T /tmp -S 50G --discount_fallback --prune 0 0 0 0 0 1 1 1 2 3 -o 20 trie`로 변경 
1. `python prepare_kenlm.py --dst /DATA/w2l/test/lm` 실행. token lm이면 `-ut` 추가
### 학습 결과 파일 구조
- 위의 예시에서는 `/DATA/w2l/test/lm`에 생성.
```
.
├── lexicon.txt
├── lm.arpa
├── lm.arpa.bin
└── lm_train.txt
```
---------------------------------------
## 서버 실행 예시
### 가정
1. 위의 예시처럼 학습을 했고, 음향모델로 `001_model_dev.bin`를 사용한다고 가정.
1. `wav2letter:cuda-server-latest` 이미지를 컨테이너로 띄울 때, `-v /DATA/w2l/test:/MODEL` 이렇게 volume을 mount할 예정.
1. gpu는 1번을 사용하고, `/LOG`에 log를 쌓고, 15001 port 띄움.
### 과정
1. 언어모델의 종류에 따라 `server_wrd.cfg`/`server_tkn.cfg`를 참고하여, `/DATA/w2l/test/server.cfg`를 생성.
1. 서버의 코어 숫자에 따라 `server.cfg`의 `nthread_decoder`를 설정.
1. `tokensdir`, `lexicon`, `lm`, `am`도 경로를 맞춰야되나, 예시대로 했다면 이미 맞춰진 상태. mount할 때 경로가 변하므로 `/DATA/w2l/test`를 `/MODEL`로 바꿔서 설정해야 됨.
1. `sudo docker run --gpus '"device=1"' -e LC_ALL=C.UTF-8 --name w2l-Server -p 15001:15001 -v /DATA/w2l/test:/MODEL wav2letter:cuda-server-latest` 실행