import argparse
import grpc
import torch
import os
import google.protobuf.empty_pb2 as empty

from functools import partial
from glob import glob

from utils import parallel_run
from maum.brain.w2l.w2l_pb2_grpc import SpeechToTextStub
from maum.brain.w2l.w2l_pb2 import Speech

CHUNK_SIZE = 16 * 1024


def test(wav, w2l):
    ppg_path = '{}.ppg'.format(os.path.splitext(wav)[0])
    if not os.path.exists(ppg_path):
        with grpc.insecure_channel(w2l) as channel:
            w2l_client = SpeechToTextStub(channel)

            with open(wav, 'rb') as rf:
                wav_binary = rf.read()
            speech_iter = (
                Speech(bin=wav_binary[idx:idx + CHUNK_SIZE])
                for idx in range(0, len(wav_binary), CHUNK_SIZE)
            )
            posterior_gram = w2l_client.ComputePosteriorGram(speech_iter)
            ppg = torch.tensor(posterior_gram.logit)
            ppg = ppg.view(posterior_gram.t, posterior_gram.n)
            torch.save(ppg, ppg_path)


def test_batch(wav_list, n_cpu, **kargv):
    test_fn = partial(test, **kargv)

    parallel_run(test_fn, wav_list,
                 desc="compute_posterior_gram", n_cpu=n_cpu)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--w2l',
                        nargs='?',
                        help='w2l grpc server=ip:port',
                        type=str,
                        default='127.0.0.1:15001')
    parser.add_argument('-i', '--input',
                        nargs='?',
                        help='input dir',
                        type=str,
                        required=True)
    parser.add_argument('-n', '--n_cpu',
                        type=int,
                        default=1,
                        help='number of processes')

    args = parser.parse_args()

    input_path = os.path.join(args.input, '**/*.wav')
    wav_list = glob(input_path, recursive=True)

    with grpc.insecure_channel(args.w2l) as channel:
        w2l_client = SpeechToTextStub(channel)
        posterior_gram_info = w2l_client.GetPosteriorGramInfo(empty.Empty())
    print("Tokens")
    for token in posterior_gram_info.tokens:
        print('\t', token)
    test_batch(wav_list, args.n_cpu,
               w2l=args.w2l)


if __name__ == '__main__':
    main()
