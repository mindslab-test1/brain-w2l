import argparse
import os

from functools import partial
from glob import glob
from utils import parallel_run, ler, wer


def test(txt, lang, encoding):
    prediction_paht = '{}.result'.format(os.path.splitext(txt)[0])

    with open(prediction_paht, 'r', encoding=encoding) as rf:
        prediction = rf.read()

    with open(txt, 'r', encoding=encoding) as rf:
        target = rf.read()

    letter_err_cnt, letter_target_cnt = ler(prediction, target, lang)
    word_err_cnt, word_target_cnt = wer(prediction, target, lang)
    if letter_target_cnt == 0:
        letter_target_cnt = 1
    if word_target_cnt == 0:
        word_target_cnt = 1

    print('ler: {:.2f}%, wer: {:.2f}%, file: {}'.format(
        letter_err_cnt / letter_target_cnt * 100,
        word_err_cnt / word_target_cnt * 100,
        txt)
    )

    return letter_err_cnt, letter_target_cnt, word_err_cnt, word_target_cnt


def test_batch(txt_list, n_cpu, **kargv):
    test_fn = partial(test, **kargv)

    results = parallel_run(test_fn, txt_list,
                           desc="test", n_cpu=n_cpu)
    letter_err_tot_cnt = 0
    letter_target_tot_cnt = 0
    word_err_tot_cnt = 0
    word_target_tot_cnt = 0
    for letter_err_cnt, letter_target_cnt, word_err_cnt, word_target_cnt in results:
        letter_err_tot_cnt += letter_err_cnt
        letter_target_tot_cnt += letter_target_cnt
        word_err_tot_cnt += word_err_cnt
        word_target_tot_cnt += word_target_cnt
    letter_err_rate = letter_err_tot_cnt / letter_target_tot_cnt
    word_err_rate = word_err_tot_cnt / word_target_tot_cnt
    print('letter error rate: {:.2f}%, word error rate: {:.2f}%'.format(
        letter_err_rate * 100,
        word_err_rate * 100
    ))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input',
                        nargs='?',
                        help='input dir',
                        type=str,
                        required=True)
    parser.add_argument('-l', '--lang',
                        type=str,
                        default='kor',
                        help='language(kor, eng)')
    parser.add_argument('-e', '--encoding',
                        type=str,
                        default='utf-8',
                        help='txt file encoding')
    parser.add_argument('-n', '--n_cpu',
                        type=int,
                        default=1,
                        help='number of processes')
    args = parser.parse_args()

    input_path = os.path.join(args.input, '**/*.txt')
    txt_list = glob(input_path, recursive=True)

    test_batch(txt_list, args.n_cpu, lang=args.lang, encoding=args.encoding)
