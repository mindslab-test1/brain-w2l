## calculate_ler.py
- input 디렉토리에서 txt 파일의 정답과 result 파일의 인식결과를 이용하여 LER를 출력하는 코드. 
- argument

|명칭       |설명                                      |필수 여부| 기본값      |
| :--------| :--------------------------------------- |:---:|:--------------|
| input    | 음성 파일과 전사 데이터가 있는 디렉토리 경로 | O | 
| lang     | text의 언어                               | X | kor
| encoding | txt file encoding                        | X | utf-8
| n_cpu    | ler를 계산할 때 사용할 process의 수        | X | 1

## batch_client.py
- input 디렉토리에 있는 음성을 gRPC 서버로 보내서 인식하여 result 확장자로 output 디렉토리에 저장하고, 
txt 파일에 있는 정답과 비교하여 LER를 출력하는 코드.
- argument

|명칭       |설명                                       |필수 여부| 기본값      |
| :--------| :---------------------------------------- |:---:|:--------------|
| w2l      | w2l gRPC 서버의 ip:port                    | X | 127.0.0.1:15001
| input    | 음성 파일과 전사 데이터가 있는 디렉토리 경로 | O | 
| lang     | text의 언어                                | X | kor
| use_txt  | 정답 사용 여부                             | X | 
| encoding | txt file encoding                         | X | utf-8
| output   | 음성 인식 결과를 출력할 위치                | X | ouput
| n_cpu    | 음성을 인식할 때 사용할 process의 수        | X | 1

## client.py
- grpc 서버에 음성 파일을 보내서 음성을 인식하여 텍스트를 출력하는 코드.

- argument

|명칭       |설명                        |필수 여부| 기본값      |
| :--------| :------------------------- |:---:|:--------------|
| remote   | w2l gRPC 서버의 ip:port     | X | 127.0.0.1:15001
| input    | 음성 파일의 경로             | O | 


## utils.py
- 공통적으로 사용하는 함수가 있는 코드