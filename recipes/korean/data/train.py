import os
import torch
import random
import subprocess
from glob import glob

from prepare_am import prepare_data
from utils import kor_characters, eng_characters
from remake_noise_lst import remake_batch


def make_data_lst(data_dir_list, samplerate, lang):
    cpu_count = os.cpu_count()
    data_lst_list = {}
    for data_name, data_dir in data_dir_list.items():
        data_lst = os.path.join(data_dir, f'data_{samplerate}.lst')
        if not os.path.exists(data_lst):
            data_path = os.path.join(data_dir, '**/*.txt')
            data_list = glob(data_path, recursive=True)
            random.shuffle(data_list)
            prepare_data(data_list, data_name, cpu_count, data_lst, lang=lang, encoding='utf-8',
                         max_duration=0, sample_rate=samplerate, use_eos=True, root_path=data_dir)
        else:
            print(f'use {data_lst}')
        data_lst_list[data_name] = data_lst
    return data_lst_list


def make_tokens_file(result_dir, lang):
    tokens_file = os.path.join(result_dir, 'am/tokens.txt')
    with open(tokens_file, 'w', encoding='utf-8') as f:
        f.write("|\n")
        if lang == 'kor':
            characters = kor_characters
        elif lang == 'eng':
            characters = eng_characters
        else:
            raise RuntimeError('{} is the wrong language.'.format(lang))
        for char in characters:
            f.write(char + "\n")


def make_lexicon_file(result_dir, word_set):
    lexicon_file = os.path.join(result_dir, 'am/lexicon.txt')
    with open(lexicon_file, 'w', encoding='utf-8') as lf:
        for word in word_set:
            if 0 < len(word):
                lf.write(
                    "{word}\t{tokens} |\n".format(word=word, tokens=" ".join(list(word)))
                )


def make_train_dev_lst(data_lst_list, lang, use_eos, max_duration, validation_rate, result_dir):
    dev_tot_list = []
    train_lst_list = []
    dev_lst_list = {}
    word_set = set()
    for data_name, data_lst in data_lst_list.items():
        data_list = []
        with open(data_lst, 'r', encoding='utf-8') as rf:
            data_dir = os.path.dirname(data_lst)
            for line in rf.readlines():
                data = line.split(' ')
                data[0] = os.path.join(data_dir, data[0])
                data[1] = os.path.join(data_dir, data[1])
                if max_duration < float(data[2]):
                    print('{} is too long.'.format(data[1]))
                if not use_eos:
                    text = ' '.join(data[3:]).replace(' $ ', ' ')
                    line = ' '.join([data[0], data[1], data[2], text])
                else:
                    line = ' '.join(data)
                word_set.update([word for word in line.strip().split(' ')[3:]])
                data_list.append(line)

        dev_len = int(len(data_list) * validation_rate)
        if  dev_len == 0:
            dev_len = 1

        train_list = data_list[dev_len:]
        train_path = os.path.join(result_dir, f'train_{data_name}.lst')
        with open(train_path, 'w', encoding='utf-8') as wf:
            wf.writelines(train_list)
        train_lst_list.append(train_path)

        dev_list = data_list[:dev_len]
        dev_path = os.path.join(result_dir, f'dev_{data_name}.lst')
        with open(dev_path, 'w', encoding='utf-8') as wf:
            wf.writelines(dev_list)
        dev_lst_list[data_name] = dev_path

        dev_tot_list.extend(dev_list)

    if 1 < len(data_lst_list):
        dev_path = os.path.join(result_dir, 'dev_tot.lst')
        with open(dev_path, 'w', encoding='utf-8') as wf:
            wf.writelines(dev_tot_list)
        dev_lst_list['tot'] = dev_path

    am_dir = os.path.join(result_dir, 'am')
    if not os.path.exists(am_dir):
        os.mkdir(am_dir)
    make_lexicon_file(result_dir, word_set)
    make_tokens_file(result_dir, lang)
    return train_lst_list, dev_lst_list


def make_noise_lst(noise_list, samplerate, result_dir):
    cpu_count = os.cpu_count()
    noise_lst_list = []
    for noise_idx, noise_dir in enumerate(noise_list):
        noise_lst = os.path.join(noise_dir, f'noise_{samplerate}.lst')
        if not os.path.exists(noise_lst):
            noise_path = os.path.join(noise_dir, '**/*.wav')
            noise_list = glob(noise_path, recursive=True)
            remake_batch(noise_list, f'make {noise_lst}', cpu_count, noise_lst,
                         samplerate=samplerate, root_path=noise_dir)
        else:
            print(f'use {noise_lst}')
        result_lst = os.path.join(result_dir, f'noise_{noise_idx}.lst')
        with open(noise_lst, 'r', encoding='utf-8') as rf, open(result_lst, 'w', encoding='utf-8') as wf:
            for line in rf.readlines():
                data = line.split(' ')
                data[0] = os.path.join(noise_dir, data[0])
                data[1] = os.path.join(noise_dir, data[1])
                wf.write(' '.join(data))
        noise_lst_list.append(result_lst)
    return noise_lst_list


def train(
        result_dir, baseline,
        lang, samplerate, use_eos,
        train_lst_list, dev_lst_list,
        epoch, batch_size,
        noise_lst_list, min_noise_sample, max_noise_sample, min_snr, max_snr):
    dev_lst_list = ','.join([f'dev-{name}:{dev_lst}' for name, dev_lst in dev_lst_list.items()])
    train_lst_list = ','.join(train_lst_list)
    noise_lst_list = ','.join(noise_lst_list)

    if lang == 'kor' and use_eos:
        replabel = 0
    else:
        replabel = 2

    if lang == 'eng':
        lexicon = os.path.join(result_dir, 'am/lexicon.txt')
        usenormalizer = False
        usemecab = False
    else:
        lexicon = ''
        usenormalizer = True
        usemecab = True

    gpu_count = torch.cuda.device_count()
    if 1 < gpu_count:
        cmd = f'mpirun -n {gpu_count} --allow-run-as-root /root/wav2letter/build/Train fork {baseline} --enable_distributed'
    elif gpu_count == 1:
        cmd = f'/root/wav2letter/build/Train fork {baseline} --noenable_distributed'
    else:
        raise RuntimeError(f'gpu_count: {gpu_count}')

    tokensdir = os.path.join(result_dir, 'am')
    args = cmd.split(' ')
    args.extend([
        '--flagsfile=/root/wav2letter/recipes/korean/train.cfg',
        '--logtostderr=1',
        f'--tokensdir={tokensdir}',
        f'--train={train_lst_list}',
        f'--valid={dev_lst_list}',
        f'--noise={noise_lst_list}',
        f'--rundir={result_dir}',
        f'--iter={epoch}',
        f'--usenormalizer={usenormalizer}',
        f'--usemecab={usemecab}',
        f'--lexicon={lexicon}',
        f'--batchsize={batch_size}',
        f'--eostoken={use_eos}',
        f'--replabel={replabel}',
        f'--samplerate={samplerate}',
        f'--minnoisesample={min_noise_sample}',
        f'--maxnoisesample={max_noise_sample}',
        f'--minsnr={min_snr}',
        f'--maxsnr={max_snr}'
    ])
    subprocess.run(args)


def main():
    lang = os.environ['W2L_LANG']
    samplerate = int(os.environ['SAMPLERATE'])
    data_list = os.environ['DATA_LIST']
    data_list = dict(data.split(':') for data in data_list.split(','))

    print('1. make data lst')
    data_lst_list = make_data_lst(data_list, samplerate, lang)

    use_eos = (os.environ['USE_EOS'] == 'true')
    max_duration = int(os.environ['MAX_DURATION'])
    validation_rate = float(os.environ['VALIDATION_RATE'])
    result_dir = os.environ['RESULT_DIR']

    print('2. make train/dev lst')
    train_lst_list, dev_lst_list = make_train_dev_lst(
        data_lst_list, lang, use_eos, max_duration, validation_rate, result_dir)

    print('3. make noise lst')
    noise_list = os.environ['NOISE_LIST']
    if noise_list == '':
        noise_lst_list = []
    else:
        noise_list = [noise for noise in noise_list.split(',')]
        noise_lst_list = make_noise_lst(noise_list, samplerate, result_dir)

    epoch = os.environ['EPOCH']
    batch_size = os.environ['BATCH_SIZE']
    min_noise_sample = os.environ['MIN_NOISE_SAMPLE']
    max_noise_sample = os.environ['MAX_NOISE_SAMPLE']
    min_snr = os.environ['MIN_SNR']
    max_snr = os.environ['MAX_SNR']
    baseline = os.environ['BASELINE']
    print('4. train am')
    train(
        result_dir, baseline,
        lang, samplerate, use_eos,
        train_lst_list, dev_lst_list,
        epoch, batch_size,
        noise_lst_list, min_noise_sample, max_noise_sample, min_snr, max_snr
    )


if __name__ == '__main__':
    main()
