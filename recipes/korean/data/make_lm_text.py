import os
import random
import argparse

from glob import glob
from utils import preprocess_text


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-t', '--txt_path', type=str, default='',
                        help='txt data path')
    parser.add_argument('--train_lst', type=str, default='',
                        help='train lst path')
    parser.add_argument('--dev_lst', type=str, default='',
                        help='dev lst path')
    parser.add_argument('-b', '--baseline', type=str, default='',
                        help='baseline lm.txt')
    parser.add_argument('-r', '--rate', type=int, default=1,
                        help='if baseline is not "", txt data is duplicated at this rate.')
    parser.add_argument('-l', '--lang', type=str, default='kor',
                        help='language(kor, eng)')
    parser.add_argument('-ut', '--use_tkn_lm', action='store_true',
                        help='use token language model')
    parser.add_argument('-ue', '--use_eos', action='store_true',
                        help='use eos(end of sentence) token')
    parser.add_argument('-e', '--encoding', type=str, default='utf-8',
                        help='txt file encoding')
    parser.add_argument('-o', '--output', type=str, default='output',
                        help='output path')
    args = parser.parse_args()

    train_output = os.path.join(args.output, 'lm_train.txt')
    with open(train_output, 'w', encoding='utf-8') as wf:
        if args.baseline != '':
            rate = args.rate
            with open(args.baseline, 'r', encoding='utf-8') as rf:
                for line in rf.readlines():
                    line = line.replace('$', '\n')
                    text = preprocess_text(
                        line,
                        lang=args.lang, use_tkn_lm=args.use_tkn_lm, is_am=False, use_eos=args.use_eos)
                    if len(text) > 0:
                        wf.write(text)
                        wf.write('\n')
        else:
            rate = 1

        if args.txt_path != '':
            txt_path = os.path.join(args.txt_path, '**/*.txt')
            txt_file_list = glob(txt_path, recursive=True)

            for txt_file in txt_file_list:
                with open(txt_file, 'r', encoding=args.encoding) as rf:
                    text = rf.read()
                    text = preprocess_text(
                        text,
                        lang=args.lang, use_tkn_lm=args.use_tkn_lm, is_am=False, use_eos=args.use_eos)
                    if len(text) > 0:
                        for _ in range(rate):
                            wf.write(text)
                            wf.write('\n')

        if args.train_lst != '':
            train_file_list = args.train_lst.split(',')
            for train_file in train_file_list:
                with open(train_file, 'r', encoding='utf-8') as rf:
                    for line in rf.readlines():
                        text = " ".join(line.strip().split(" ")[3:])
                        text = text.replace('$', '\n')
                        if args.lang == 'kor':
                            text = text.replace(' #', '')

                        text = preprocess_text(
                            text,
                            lang=args.lang, use_tkn_lm=args.use_tkn_lm, is_am=False, use_eos=args.use_eos)
                        if len(text) > 0:
                            for _ in range(rate):
                                wf.write(text)
                                wf.write('\n')

    if args.dev_lst != '':
        dev_data = []
        dev_file_list = args.dev_lst.split(',')
        for dev_file in dev_file_list:
            with open(dev_file, 'r', encoding='utf-8') as rf:
                for line in rf.readlines():
                    text = " ".join(line.strip().split(" ")[3:])
                    text = text.replace('$', '\n')
                    if args.lang == 'kor':
                        text = text.replace(' #', '')

                    text = preprocess_text(
                        text,
                        lang=args.lang, use_tkn_lm=args.use_tkn_lm, is_am=False, use_eos=args.use_eos)
                    text = text + "\n"
                    dev_data.append(text)

        random.shuffle(dev_data)

        valid_data = dev_data[:int(len(dev_data)/2)]
        valid_output = os.path.join(args.output, 'lm_valid.txt')
        with open(valid_output, 'w', encoding='utf-8') as wf:
            wf.writelines(valid_data)

        test_data = dev_data[int(len(dev_data)/2):]
        test_output = os.path.join(args.output, 'lm_test.txt')
        with open(test_output, 'w', encoding='utf-8') as wf:
            wf.writelines(test_data)
