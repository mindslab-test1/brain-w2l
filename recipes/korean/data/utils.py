import re
import konlpy.tag as nlp

from tqdm import tqdm
from contextlib import closing
from multiprocessing import Pool
from unicodedata import normalize

nlp = nlp.Mecab()

_jamo_leads = "".join([chr(_) for _ in range(0x1100, 0x1113)])
_jamo_vowels = "".join([chr(_) for _ in range(0x1161, 0x1176)])
_jamo_tails = "".join([chr(_) for _ in range(0x11A8, 0x11C3)])
kor_characters = '#' + _jamo_leads + _jamo_vowels + _jamo_tails
eng_characters = '\'' + "".join([chr(alphabet) for alphabet in range(ord("a"), ord("z") + 1)])


def transform_asg(word):
    if word == "":
        return ""
    new_word = word[0]
    prev = word[0]
    repetition = 0
    for letter in word[1:]:
        if letter == prev:
            repetition += 1
        else:
            if repetition != 0:
                new_word += "1" if repetition == 1 else "2"
                repetition = 0
            new_word += letter
        prev = letter
    if repetition != 0:
        new_word += "1" if repetition == 1 else "2"
    return new_word


def preprocess_sentence(text, lang='kor', use_tkn_lm=False, is_am=True):
    if lang == 'kor':
        text = re.sub(r'[^가-힣 ]+', r' ', text)
        text = re.sub(r'\s+', r' ', text)
        text = text.strip()
        morphs = nlp.morphs(text)
        word_list = []
        start_idx = 0
        for morph in morphs:
            morph = morph.strip()
            find_idx = text.find(morph, start_idx)
            start_idx = find_idx + len(morph)
            if find_idx > 0 and text[find_idx - 1] != ' ':
                morph = "#" + morph
            word_list.append(morph)

        text = ' '.join(word_list)
        if is_am:
            text = normalize('NFKC', text)
        else:
            text = normalize('NFKD', text)
    elif lang == 'eng':
        text = text.lower()
        text = re.sub(r'[^a-z\' ]+', r' ', text)
        text = re.sub(r'\s+', r' ', text)
        text = text.strip()
    else:
        raise RuntimeError('{} is the wrong language.'.format(lang))
    if use_tkn_lm:
        words = text.split(" ")
        word_list = []
        for word in words:
            new_word = transform_asg(word) + "|"
            new_word = " ".join(list(new_word)) + " "
            word_list.append(new_word)
        text = ''.join(word_list)
    return text


def preprocess_text(text, lang='kor', use_tkn_lm=False, is_am=True, use_eos=False):
    text = text.replace(u"\ufeff", '')
    if use_eos:
        text = re.sub(r' *\n+ *', '\n', text)
        text_eos = []
        for sentence in text.split('\n'):
            sentence = sentence.strip()
            if len(sentence) > 0:
                sentence = preprocess_sentence(sentence, lang, use_tkn_lm, is_am).strip()
                text_eos.append(sentence)
        if use_tkn_lm:
            text_eos = ' $ | '.join(text_eos)
        else:
            text_eos = ' $ '.join(text_eos)
        return text_eos
    else:
        return preprocess_sentence(text, lang, use_tkn_lm, is_am)


def parallel_run(fn, items, desc="", n_cpu=1):
    results = []

    if n_cpu > 1:
        with closing(Pool(n_cpu)) as pool:
            for out in tqdm(pool.imap_unordered(
                    fn, items), total=len(items), desc=desc):
                if out is not None:
                    results.append(out)
    else:
        for item in tqdm(items, total=len(items), desc=desc):
            out = fn(item)
            if out is not None:
                results.append(out)

    return results
