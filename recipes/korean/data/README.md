## prepare_am.py
- raw data를 음향모델(Acoustic Model, AM)에서 학습할 때 사용할 수 있는 형태로 만드는 코드.
- argument

|명칭          |설명                                                                          |필수 여부|기본값|
| :------------| :-------------------------------------------------------------------------- |:---:|:-------|
| train        | 학습할 데이터가 있는 디렉토리 경로                                             | O | 
| dev          | validation 데이터가 있는 디렉토리 경로                                        | X | 
| rate         | dev 값이 비어있을 때 trian data 중에서 랜덤하게 validation data로 추출하는 비율 | X | 0.1
| lang         | 전사(transcription)한 text의 언어                                            | X | kor
| use_eos      | eos(end of sentence) token을 사용                                          | X |
| encoding     | 전사(transcription)한 text의 file encoding                                   | X | utf-8
| max_duration | 음성의 최대 길이(millisecond), 이 길이 이상되는 음성은 미사용                   | X | 30000.0
| sample_rate  | 음성의 샘플링레이트, 음성의 샘플링레이트가 이 수치와 다르면 미사용                | X | 16000
| n_cpu        | 전처리를 돌릴 때 사용할 process의 수                                          | X | 1
| output       | 전처리 결과를 출력할 위치                                                     | X | ouput

- output 디렉토리에 생성되는 파일 구조
```
.
├── am
│   ├── lexicon.txt
│   └── tokens.txt
├── dev.lst
├── model
└── train.lst
```

## make_lm_text.py
- raw data를 ken lm에서 학습할 때 사용할 수 있는 형태로 만드는 코드.
- argument

|명칭        |설명                                                       |필수 여부|기본값|
| :----------| :-------------------------------------------------------- |:---:|:-------|
| txt_path   | 학습할 데이터가 있는 디렉토리 경로                          | X | 
| train_lst  | am을 학습할 때 사용한 파일의 경로(comma-separated)         | X |
| dev_lst    | am을 검증할 때 사용한 파일의 경로(comma-separated)         | X |
| baseline   | baseline LM을 만들 때 사용한 lm.txt의 위치                 | X | 
| rate       | baseline data가 존재할 때, 학습할 데이터를 몇 배 늘릴지 결정 | X | 1
| lang       | text의 언어                                               | X | kor
| use_tkn_lm | token language model을 사용                               | X | 
| use_eos    | eos(end of sentence) token을 사용                         | X |
| encoding   | txt file encoding                                         | X | utf-8
| output     | 전처리 결과를 출력할 위치                                   | X | ouput

- output 디렉토리에 생성되는 파일 구조
```
.
├── lm_train.txt
├── lm_valid.txt
└── lm_test.txt
```

## prepare_kenlm.py
- ken lm을 돌린 결과를 언어 모델(Language Model, LM)로 사용할 수 있게 만드는 코드.
- argument

|명칭        |설명                             |필수 여부|기본값|
| :----------| :------------------------------ |:---:|:-------|
| dst        | lm.arpa 파일있는 디렉토리 경로   | O |
| lang       | text의 언어                    | X | kor 
| use_tkn_lm | token language model을 사용    | X |
| kenlm      | kenlm이 설치된 경로             | X | /root/kenlm

- dst 디렉토리에 생성되는 파일 구조
```
.
├── lexicon.txt
└── lm.arpa.bin
```

## remake_eos_lst.py
- eos(end of sentence) token이 없는 train/dev.lst 파일을 eos token이 있는 lst 파일로 변환하는 코드
- argument

|명칭       |설명                                         |필수 여부|기본값|
| :---------| :----------------------------------------- |:---:|:-------|
| input     | train/dev.lst 파일의 경로                   | O | 
| lang      | 전사(transcription)한 text의 언어           | X | kor
| encoding  | 전사(transcription)한 text의 file encoding  | X | utf-8
| n_cpu     | 전처리를 돌릴 때 사용할 process의 수          | X | 1
| output    | lst 파일을 출력할 경로                       | O | 
-example
```
python remake_eos_lst.py -i train.lst -o train_eos.lst -e cp949 -n 32 -l kor
```
## remake_noise_lst.py
- 예전에 만들어진 noise.lst 파일을 최신화하는 코드
- argument

|명칭       |설명                                  |필수 여부|기본값|
| :---------| :---------------------------------- |:---:|:-------|
| input     | noise.lst 파일의 경로                | O | 
| n_cpu     | 전처리를 돌릴 때 사용할 process의 수  | X | 1
| output    | noise.lst 파일을 출력할 경로         | O | 
-example
```
python remake_noise_lst.py -i noise_old.lst -o noise.lst -n 32
```

## utils.py
- 공통적으로 사용하는 함수가 있는 코드