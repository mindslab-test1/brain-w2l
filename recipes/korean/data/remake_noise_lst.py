import argparse
import sox
import os
import re
from functools import partial
from utils import parallel_run


def remake(data, samplerate=0, root_path=None):
    noise_file = data.replace('\n', '')
    data_id = os.path.splitext(noise_file)[0]
    full_path = os.path.abspath(noise_file)
    duration = sox.file_info.duration(full_path) * 1000

    file_sample_rate = sox.file_info.sample_rate(full_path)
    if file_sample_rate < samplerate:
        return None
    if 0 < samplerate != file_sample_rate:
        new_full_path = '{}_sr{}.wav'.format(os.path.splitext(full_path)[0], samplerate)
        if os.path.exists(new_full_path):
            return None
        tfm = sox.Transformer()
        tfm.convert(samplerate=samplerate)
        tfm.build(full_path, new_full_path)
        print('resample: {} -> {}'.format(full_path, new_full_path))
        full_path = new_full_path
    if root_path is not None:
        root_path_pattern = '^' + root_path + '/'
        root_path_pattern = re.sub(r'/+', '/', root_path_pattern)
        data_id = re.sub(root_path_pattern, '', data_id)
        full_path = re.sub(root_path_pattern, '', full_path)
    return ' '.join([data_id, full_path, str(duration)]) + ' \n'


def remake_batch(data_list, desc, n_cpu, output, **kargv):
    fn = partial(remake, **kargv)

    with open(output, 'w', encoding='utf-8') as wf:
        for data in parallel_run(fn, data_list, desc=desc, n_cpu=n_cpu):
            if data is not None:
                wf.write(data)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input',
                        nargs='?',
                        help='input noise.lst file',
                        type=str,
                        required=True)
    parser.add_argument('-o', '--output',
                        nargs='?',
                        help='output noise.lst file',
                        type=str,
                        required=True)
    parser.add_argument('-n', '--n_cpu', type=int, default=1,
                        help='number of processes')

    args = parser.parse_args()

    with open(args.input, 'r', encoding='utf-8') as rf:
        lines = rf.readlines()
        remake_batch(lines, "remake noise.lst file", args.n_cpu, args.output, samplerate=0)
