import sox
import os
import argparse
import random
import re

from functools import partial
from glob import glob
from utils import preprocess_text, parallel_run, kor_characters, eng_characters


def make_lst(txt_file, lang, encoding, max_duration, sample_rate, use_eos, root_path=None):
    with open(txt_file, 'r', encoding=encoding) as rf:
        text = rf.read()
    text = preprocess_text(text,
                           lang=lang, use_eos=use_eos)
    if len(text) == 0:
        return None

    data_id = os.path.splitext(txt_file)[0]
    full_path = os.path.abspath(txt_file)
    full_path = '{}.wav'.format(os.path.splitext(full_path)[0])
    if not os.path.exists(full_path):
        print('{} does not exist.'.format(full_path))
        return None
    duration = sox.file_info.duration(full_path) * 1000
    if 0 < max_duration < duration:
        print('{} is too long.'.format(full_path))
        return None
    file_sample_rate = sox.file_info.sample_rate(full_path)
    if sample_rate != file_sample_rate:
        tfm = sox.Transformer()
        tfm.convert(samplerate=sample_rate)
        new_full_path = '{}_sr{}.wav'.format(os.path.splitext(full_path)[0], sample_rate)
        tfm.build(full_path, new_full_path)
        print('resample: {} -> {}'.format(full_path, new_full_path))
        full_path = new_full_path
    if root_path is not None:
        root_path_pattern = '^' + root_path + '/'
        root_path_pattern = re.sub(r'/+', '/', root_path_pattern)
        data_id = re.sub(root_path_pattern, '', data_id)
        full_path = re.sub(root_path_pattern, '', full_path)
    return ' '.join([data_id, full_path, str(duration), text]), text


def prepare_data(data_list_, data_name, n_cpu, output, **kargv):
    fn = partial(make_lst, **kargv)

    word_set = set()
    with open(output, 'w', encoding='utf-8') as tf:
        for data in parallel_run(fn, data_list_, desc=f"preprocessing {data_name} file", n_cpu=n_cpu):
            if data is not None:
                lst_data, text = data
                word_list = text.split(' ')
                word_set.update(word_list)
                tf.write(lst_data)
                tf.write('\n')
    return word_set


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-t', '--train', type=str, required=True,
                        help='train data path')
    parser.add_argument('-d', '--dev', type=str, default="",
                        help='dev data path')
    parser.add_argument('-r', '--rate', type=float, default=0.1,
                        help='if dev="", dev is generated at this rate.')
    parser.add_argument('-l', '--lang', type=str, default='kor',
                        help='language(kor, eng)')
    parser.add_argument('-ue', '--use_eos', action='store_true',
                        help='use eos(end of sentence) token')
    parser.add_argument('-e', '--encoding', type=str, default='utf-8',
                        help='txt file encoding')
    parser.add_argument('-m', '--max_duration', type=float, default=30000.0,
                        help='maximum duration of audio file(ms)')
    parser.add_argument('-s', '--sample_rate', type=int, default=16000,
                        help='sample rate of audio file(hz)')
    parser.add_argument('-n', '--n_cpu', type=int, default=1,
                        help='number of processes')
    parser.add_argument('-o', '--output', type=str, default='output',
                        help='output path')
    args = parser.parse_args()

    train_path = os.path.join(args.train, '**/*.txt')
    train_list = glob(train_path, recursive=True)

    for directory in [args.output, os.path.join(args.output, 'model'), os.path.join(args.output, 'am')]:
        if not os.path.exists(directory):
            os.mkdir(directory)

    if args.dev == '':
        random.shuffle(train_list)
        dev_len = int(len(train_list) * args.rate)

        dev_list = train_list[:dev_len]
        train_list = train_list[dev_len:]
    else:
        dev_path = os.path.join(args.dev, '**/*.txt')
        dev_list = glob(dev_path, recursive=True)

    tokens_file = os.path.join(args.output, 'am/tokens.txt')
    with open(tokens_file, 'w', encoding='utf-8') as f:
        f.write("|\n")
        if args.lang == 'kor':
            characters = kor_characters
        elif args.lang == 'eng':
            characters = eng_characters
        else:
            raise RuntimeError('{} is the wrong language.'.format(args.lang))
        for char in characters:
            f.write(char + "\n")

    word_set = set()
    output = os.path.join(args.output, 'train.lst')
    train_word_set = prepare_data(
        train_list, 'train', args.n_cpu, output, lang=args.lang, encoding=args.encoding,
        max_duration=args.max_duration, sample_rate=args.sample_rate, use_eos=args.use_eos)
    word_set.update(train_word_set)

    output = os.path.join(args.output, 'dev.lst')
    dev_word_set = prepare_data(
        dev_list, 'dev', args.n_cpu, output, lang=args.lang, encoding=args.encoding,
        max_duration=args.max_duration, sample_rate=args.sample_rate, use_eos=args.use_eos)
    word_set.update(dev_word_set)

    lexicon_file = os.path.join(args.output, 'am/lexicon.txt')
    with open(lexicon_file, 'w', encoding='utf-8') as lf:
        for word in word_set:
            if 0 < len(word):
                lf.write(
                    "{word}\t{tokens} |\n".format(word=word, tokens=" ".join(list(word)))
                )


if __name__ == "__main__":
    main()
