import os
import subprocess


def make_lm_text(lang, use_eos, use_tkn_lm, encoding, txt_path, output_dir):
    cmd = [
        'python', '-u', 'make_lm_text.py',
        '--txt_path', txt_path,
        '--lang', lang,
        '--encoding', encoding,
        '--output', output_dir,
    ]
    if use_eos:
        cmd.append('--use_eos')
    if use_tkn_lm:
        cmd.append('--use_tkn_lm')
    subprocess.run(cmd)


def make_lm_arpa(use_tkn_lm, output_dir):
    cmd = [
        '/root/kenlm/build/bin/lmplz'
    ]
    if use_tkn_lm:
        cmd.extend('-T /tmp -S 20G --discount_fallback --prune 0 0 0 0 0 1 1 1 2 3 -o 20 trie'.split(' '))
    else:
        cmd.extend('--discount_fallback -o 4'.split(' '))
    lm_train_path = os.path.join(output_dir, 'lm_train.txt')
    lm_arpa_path = os.path.join(output_dir, 'lm.arpa')
    with open(lm_train_path, 'rb') as rf, open(lm_arpa_path, 'wb') as wf:
        subprocess.run(cmd, stdin=rf, stdout=wf)


def prepare_kenlm(lang, use_tkn_lm, output_dir):
    cmd = [
        'python', '-u', 'prepare_kenlm.py',
        '--lang', lang,
        '--dst', output_dir
    ]
    if use_tkn_lm:
        cmd.append('--use_tkn_lm')
    subprocess.run(cmd)


def main():
    lang = os.environ['W2L_LANG']
    use_eos = (os.environ['USE_EOS'] == 'true')
    use_tkn_lm = (os.environ['USE_TKN_LM'] == 'true')
    encoding = os.environ['DATA_ENCODING']
    txt_path = os.environ['DATA_DIR']
    output_dir = os.environ['RESULT_DIR']

    print('1. make_lm_text')
    make_lm_text(lang, use_eos, use_tkn_lm, encoding, txt_path, output_dir)

    print('2. make_lm_arpa')
    make_lm_arpa(use_tkn_lm, output_dir)

    print('3. prepare_kenlm')
    prepare_kenlm(lang, use_tkn_lm, output_dir)


if __name__ == '__main__':
    main()
