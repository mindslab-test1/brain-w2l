from __future__ import absolute_import, division, print_function, unicode_literals

import argparse
import os
import re
import sys
from utils import kor_characters, eng_characters


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Librispeech LM data creation.")
    parser.add_argument("--dst", help="destination directory", required=True)
    parser.add_argument('-l', '--lang', type=str, default='kor', help='language(kor, eng)')
    parser.add_argument('-ut', '--use_tkn_lm', action='store_true', help='use token language model')
    parser.add_argument("--kenlm", help="location to installed kenlm directory", default="/root/kenlm")

    args = parser.parse_args()

    assert os.path.isdir(str(args.kenlm)), "kenlm directory not found - '{d}'".format(
        d=args.kenlm
    )

    lm_dir = os.path.join(args.dst)
    os.makedirs(lm_dir, exist_ok=True)

    arpa_file = os.path.join(lm_dir, 'lm.arpa')
    # temporary arpa file in lowercase
    sys.stdout.write("\nSaving ARPA LM file in binary format ...\n\n")
    sys.stdout.flush()
    os.system(
        "cat {arpa} | tr '[:upper:]' '[:lower:]' > {arpa}.tmp".format(arpa=arpa_file)
    )
    binary = os.path.join(args.kenlm, "build", "bin", "build_binary")
    os.system("{bin} {i}.tmp {o}".format(bin=binary, i=arpa_file, o=arpa_file + ".bin"))
    os.remove("{arpa}.tmp".format(arpa=arpa_file))

    if not args.use_tkn_lm:
        # write words to lexicon.txt file
        dict_file = os.path.join(lm_dir, "lexicon.txt")
        sys.stdout.write("\nWriting Lexicon file - {d}...\n\n".format(d=dict_file))
        sys.stdout.flush()

        if args.lang == 'kor':
            characters = kor_characters
        elif args.lang == 'eng':
            characters = eng_characters
        else:
            raise RuntimeError('{} is the wrong language.'.format(args.lang))

        with open(dict_file, "w") as f:
            # get all the words in the arpa file
            with open(arpa_file, "r") as arpa:
                for line in arpa:
                    # verify if the line corresponds to unigram
                    if not re.match(r"[-]*[0-9\.]+\t\S+\t*[-]*[0-9\.]*$", line):
                        continue
                    word = line.split("\t")[1]
                    word = word.strip().lower()
                    if word == "<unk>" or word == "<s>" or word == "</s>":
                        continue
                    assert re.match("^[{}]+$".format(characters), word), "invalid word - {w}".format(w=word)
                    f.write("{w}\t{s} |\n".format(w=word, s=" ".join(word)))
    sys.stdout.write("Done !\n")
