import os
import argparse
from functools import partial
from utils import preprocess_text, parallel_run


def remake(data, encoding, lang):
    data = data.strip().split(' ')[:3]
    txt_path = '{}.txt'.format(os.path.splitext(data[1])[0])
    with open(txt_path, 'r', encoding=encoding) as tf:
        text = tf.read()

    text = preprocess_text(text,
                           lang=lang, use_eos=True)
    data.append(text)
    return data


def remake_batch(data_list, n_cpu, output, **kargv):
    fn = partial(remake, **kargv)

    with open(output, 'w', encoding='utf-8') as wf:
        for data in parallel_run(fn, data_list, desc="remake data.lst file", n_cpu=n_cpu):
            wf.write(' '.join(data))
            wf.write('\n')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input',
                        nargs='?',
                        help='input lst file',
                        type=str,
                        required=True)
    parser.add_argument('-l', '--lang',
                        type=str,
                        default='kor',
                        help='language(kor, eng)')
    parser.add_argument('-e', '--encoding',
                        type=str,
                        default='utf-8',
                        help='txt file encoding')
    parser.add_argument('-o', '--output',
                        nargs='?',
                        help='output lst file',
                        type=str,
                        required=True)
    parser.add_argument('-n', '--n_cpu', type=int, default=1,
                        help='number of processes')

    args = parser.parse_args()

    with open(args.input, 'r', encoding='utf-8') as rf:
        lines = rf.readlines()
        remake_batch(lines, args.n_cpu, args.output,
                     encoding=args.encoding, lang=args.lang)
